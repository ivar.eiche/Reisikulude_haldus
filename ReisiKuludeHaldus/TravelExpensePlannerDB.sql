CREATE DATABASE  IF NOT EXISTS `UserDB` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `UserDB`;
-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: localhost    Database: UserDB
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-26 16:47:55
CREATE DATABASE  IF NOT EXISTS `form` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `form`;
-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: localhost    Database: form
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `user` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES ('Admin','passw0rd');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-26 16:47:55
CREATE DATABASE  IF NOT EXISTS `ReisiKuludePlaneerimine` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `ReisiKuludePlaneerimine`;
-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: localhost    Database: ReisiKuludePlaneerimine
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accommodation`
--

DROP TABLE IF EXISTS `accommodation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accommodation` (
  `accommodationID` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(45) NOT NULL,
  `check-in` date NOT NULL,
  `check-out` date NOT NULL,
  `cost` decimal(10,0) NOT NULL,
  PRIMARY KEY (`accommodationID`),
  UNIQUE KEY `idMAJUTUS_UNIQUE` (`accommodationID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accommodation`
--

LOCK TABLES `accommodation` WRITE;
/*!40000 ALTER TABLE `accommodation` DISABLE KEYS */;
INSERT INTO `accommodation` VALUES (1,'London, UK','2001-01-20','2002-01-20',2500),(2,'Los Angeles, USA','2004-01-20','2014-01-20',3000),(3,'Tartu, EST','2007-01-20','2008-01-20',4253),(4,'Helsinki, FIN','2012-12-20','2014-12-20',4262);
/*!40000 ALTER TABLE `accommodation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accountants`
--

DROP TABLE IF EXISTS `accountants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountants` (
  `accountantsID` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `social_security` varchar(11) NOT NULL,
  `occupationID` int(3) NOT NULL,
  PRIMARY KEY (`accountantsID`),
  UNIQUE KEY `accountantID_UNIQUE` (`accountantsID`),
  UNIQUE KEY `social_security_UNIQUE` (`social_security`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accountants`
--

LOCK TABLES `accountants` WRITE;
/*!40000 ALTER TABLE `accountants` DISABLE KEYS */;
INSERT INTO `accountants` VALUES (14,'Mari','Maasikas','56092847','mari@gmail.com','49035383533',3),(15,'LaraUpdate','Croft','2345678','Lara@gmail.com','43235383533',2);
/*!40000 ALTER TABLE `accountants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `employeesID` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `email` varchar(45) NOT NULL,
  `social_security` varchar(11) NOT NULL,
  `occupationID` int(3) NOT NULL,
  `department` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`employeesID`),
  UNIQUE KEY `idTÖÖTAJAD_UNIQUE` (`employeesID`),
  UNIQUE KEY `social_security_UNIQUE` (`social_security`),
  KEY `employee_occupation_occupationID_idx` (`occupationID`),
  CONSTRAINT `employee_occupation_occupationID` FOREIGN KEY (`occupationID`) REFERENCES `occupations` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (1,'Vivek','Google','34653463','email@email.com','12345678910',1,'HR','Hired'),(2,'Ants','Tamm','56405938','ants@antson.com','23546572354',2,'Finance','Fired'),(4,'Juri','Kaalikas','56008000','jurka@kaalikas.ee','38006128913',2,'HR','On Vacation'),(5,'Aliina','Antonovna','5200500','alin4ik@gmail.com','49305268913',2,'HR','Hired'),(6,'UpdateKatsetus','DviNüüd','Whatever','mario.kendra@gmail.com','11115678913',2,'HR','On Vacation'),(8,'gfhj','fghj','fghjk','fghjk','45678978123',2,'gvbhjknk','bhjknkm'),(9,'asd','asd','asd','asd','12312312312',2,'asd','asd'),(10,'123','asd','asd','asd','44444444444',2,'hr','asdf'),(11,'hjkjlk','hbkjlk','hkjl','hbkjk','49582049581',3,'kbhj','gvjhbkn'),(13,'Maria','Tammearu','2352544','mari@narva.ee','56492065201',3,'Marketing','Fired'),(14,'Paavel','Leksikon','53452414','Pasha@mail.ru','37384712746',3,'Legal','Hired');
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extra_cost`
--

DROP TABLE IF EXISTS `extra_cost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extra_cost` (
  `extraID` int(11) unsigned zerofill NOT NULL,
  `extra_type` varchar(45) NOT NULL,
  `extra_price` double unsigned zerofill NOT NULL,
  `extra_date` varchar(45) NOT NULL,
  `reservationID` int(11) NOT NULL,
  PRIMARY KEY (`extraID`),
  UNIQUE KEY `extrasID_UNIQUE` (`extraID`),
  KEY `fk_reservations_extraCost_reservationsID_idx` (`reservationID`),
  CONSTRAINT `fk_reservations_extraCost_reservationsID` FOREIGN KEY (`reservationID`) REFERENCES `reservations` (`reservationsID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extra_cost`
--

LOCK TABLES `extra_cost` WRITE;
/*!40000 ALTER TABLE `extra_cost` DISABLE KEYS */;
/*!40000 ALTER TABLE `extra_cost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `managers`
--

DROP TABLE IF EXISTS `managers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `managers` (
  `managersID` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `social_security` varchar(11) NOT NULL,
  `occupationID` int(3) NOT NULL,
  `department` varchar(45) NOT NULL,
  PRIMARY KEY (`managersID`),
  UNIQUE KEY `managersID_UNIQUE` (`managersID`),
  UNIQUE KEY `social_security_UNIQUE` (`social_security`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `managers`
--

LOCK TABLES `managers` WRITE;
/*!40000 ALTER TABLE `managers` DISABLE KEYS */;
INSERT INTO `managers` VALUES (1,'Margatita','Marinovna','56754332','margarita@manager.com','48672349581',3,'HR'),(3,'Kalju','Merilaid','56066600','kalju.merilaid@google.com','37609225924',2,'Legal');
/*!40000 ALTER TABLE `managers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `occupations`
--

DROP TABLE IF EXISTS `occupations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `occupations` (
  `ID` int(3) NOT NULL,
  `occupation` varchar(45) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `idoccupation_UNIQUE` (`ID`),
  UNIQUE KEY `occupation_UNIQUE` (`occupation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `occupations`
--

LOCK TABLES `occupations` WRITE;
/*!40000 ALTER TABLE `occupations` DISABLE KEYS */;
INSERT INTO `occupations` VALUES (1,'accountant','manages money'),(2,'manager','manages employees'),(3,'developer','generates money');
/*!40000 ALTER TABLE `occupations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservations`
--

DROP TABLE IF EXISTS `reservations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservations` (
  `reservationsID` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `departure` varchar(45) NOT NULL,
  `destination` varchar(45) NOT NULL,
  `start_date` varchar(45) NOT NULL,
  `end_date` varchar(45) NOT NULL,
  `transportation_type` varchar(45) NOT NULL,
  `transportation_cost` decimal(10,0) NOT NULL,
  `accommodation` varchar(45) NOT NULL,
  `accommodation_cost` decimal(10,0) NOT NULL,
  `trip_description` text NOT NULL,
  `estimated_cost` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`reservationsID`),
  UNIQUE KEY `reservationID_UNIQUE` (`reservationsID`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservations`
--

LOCK TABLES `reservations` WRITE;
/*!40000 ALTER TABLE `reservations` DISABLE KEYS */;
INSERT INTO `reservations` VALUES (2,'Mario','Kendra','Tallinn, Estonia','Paris, France','2002-04-20','2009-04-20','Bus',32,'Le Cheateau de Paris',4253,'We have planned to have the best trip of our life so we are going to Paris for some business conferences and networking...',NULL),(3,'Mari','Maasikas','Helsinki, Finland','Miami, USA','2007-04-20','2015-04-20','Boat',3342,'Le Cheateau de Mongolia',42523433,'testing123, testing123, testing123, testing123, testing123, testing123, testing123, testing123, testing123, testing123',NULL),(4,'Andrus','Kivirähn','Tallinn, Estonia','Oslo, Norway','06/04/2017','10/09/2018','Plane',234567,'Four Seasons',3424,'testing',NULL),(5,'Karl','Marx','Berlin, Germany','New York, USA','2017-02-25','2017-03-18','Ship',5700,'Ritz Carlton',9999,'Networking and Social gathering ',NULL),(6,'Kristi','Kask','Paris, France','Moscow, Russia','2017-02-15','2017-02-15','Ship',346,'Backbackers Hostel',8790,'Chilling321',NULL),(7,'Tanel','Tamm','Tallinn, Estonia','Shanghai, China','2017-04-15','2017-04-25','Plane',2500,'AirBNB',995,'Networking',NULL),(8,'Helerin','Haab','Strasbourgh, France','Tallinn, Estonia','2017-02-09','2017-02-09','Car',250,'Swiss Hotel',595,'Funtime',NULL),(9,'Marina','Abramovic','Nice, France','Barcelona, Spain','2017-02-08','2017-04-07','Train',23465,'Mjäu',3456,' Test777',NULL),(10,'Maali','Mustikas','Krakow','Nürnburg','2017-04-01','2017-05-12','Bus',500,'Hotel',23,'BlaBlaBla',NULL),(11,'Jüri','Põld','Bergen, Norway','Tokyo, Japan','2017-02-25','2017-02-28','Plane',1400,'AirBNB',500,'Testing the system123',NULL),(12,'Vassili','Petrovits','Manchester, UK','Ankara, Turkey','15/05/2017','26/06/2017','Plane',600,'Marionett',1450,'Testing Update function on trip ID 12',NULL),(50,'Raimond','Kaljumäe','Brlggmany','Zglglgyrich, Switzerland','13/02/2018','04/04/2019','Plane',2342345,'Zyrdfsvg ',12422352,'wefsdghjhfds',NULL),(52,'KolmTilkaVerd','kjköl','hkjnlkm','khjnlkmöl,','17/03/2017','24/04/2018','Bus',5678,'yhujk',56789,'hbjnklm',NULL),(53,'11.46','kjköl','hkjnlkm','khjnlkmöl,','17/03/2017','24/04/2018','Plane',5678,'yhujk',5678,'testing',NULL),(55,'Ivar','TheNortalMan','Tallinn, Estonia','Tartu, Estonia','03/03/2017','28/03/2017','train',6,'Raatuse',1,'Training',NULL),(57,'23.50','kjköl','hkjnlkm','khjnlkmöl,','17/03/2017','24/04/2018','Plane',5678,'yhujk',5678,'dsfbghmfn',NULL),(59,'Dimitri','Kulkin','dfgh','fgj','20/03/2017','21/03/2017','Bus',232,'asd',342,'Random reis',NULL),(60,'Dimitri','Kulkin','dfgh','fgj','20/03/2017','21/03/2017','Bus',232,'asd',342,'random',NULL),(61,'10.09','Reverse','Bremen, Germany','Zyrich, Switzerland','25/03/2017','28/05/2017','Plane',234,'Zyrich SPA ',1242,'blablabla',NULL),(62,'10.16','krt','afdsf','jhkjlkm','01/03/2017','05/03/2017','Plane',500,'xxx',500,'yyy',NULL),(63,'10.20','krt','afdsf','jhkjlkm','01/03/2017','05/03/2017','Bus',500,'xxx',500,'xxx',NULL),(64,'13.27','krt','afdsf','jhkjlkm','01/03/2017','05/03/2017','Plane',500,'xxx',500,'yyy',NULL),(67,'15.50','zzz','zzz','zzz','16/03/2018','16/04/2018','Car',555,'zzz',555,'zzz',NULL),(70,'10.02','EstimatedCostAdded','Tallinn, Estonia','Miami, US','04/03/2017','14/03/2017','Plane',2500,'Ritz Carlton',5000,'Fun & Sun',0);
/*!40000 ALTER TABLE `reservations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transportation`
--

DROP TABLE IF EXISTS `transportation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transportation` (
  `transportationID` int(11) NOT NULL AUTO_INCREMENT,
  `transport_type` varchar(45) NOT NULL,
  `transport_from` date NOT NULL,
  `transport_to` date NOT NULL,
  `cost` decimal(10,0) NOT NULL,
  PRIMARY KEY (`transportationID`),
  UNIQUE KEY `idnew_table_UNIQUE` (`transportationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transportation`
--

LOCK TABLES `transportation` WRITE;
/*!40000 ALTER TABLE `transportation` DISABLE KEYS */;
/*!40000 ALTER TABLE `transportation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `travel_summary`
--

DROP TABLE IF EXISTS `travel_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `travel_summary` (
  `summaryID` int(11) NOT NULL AUTO_INCREMENT,
  `employee_info` varchar(45) NOT NULL,
  `trip_info` varchar(45) NOT NULL,
  `trip_description` varchar(45) NOT NULL,
  `trip_transportation` varchar(45) DEFAULT NULL,
  `trip_accommodation` varchar(45) DEFAULT NULL,
  `additional_costs` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`summaryID`),
  UNIQUE KEY `ID_UNIQUE` (`summaryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `travel_summary`
--

LOCK TABLES `travel_summary` WRITE;
/*!40000 ALTER TABLE `travel_summary` DISABLE KEYS */;
/*!40000 ALTER TABLE `travel_summary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trip_description`
--

DROP TABLE IF EXISTS `trip_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trip_description` (
  `trip_descriptionID` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  PRIMARY KEY (`trip_descriptionID`),
  UNIQUE KEY `idREISI_KIRJELDUS_UNIQUE` (`trip_descriptionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trip_description`
--

LOCK TABLES `trip_description` WRITE;
/*!40000 ALTER TABLE `trip_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `trip_description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trip_info`
--

DROP TABLE IF EXISTS `trip_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trip_info` (
  `trip_infoID` int(11) NOT NULL AUTO_INCREMENT,
  `trip_from` varchar(45) NOT NULL,
  `start_date` date NOT NULL,
  `trip_to` varchar(45) NOT NULL,
  `end_date` date NOT NULL,
  PRIMARY KEY (`trip_infoID`),
  UNIQUE KEY `idREISI_INFO_UNIQUE` (`trip_infoID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trip_info`
--

LOCK TABLES `trip_info` WRITE;
/*!40000 ALTER TABLE `trip_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `trip_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_table`
--

DROP TABLE IF EXISTS `users_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_table` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `create_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_table`
--

LOCK TABLES `users_table` WRITE;
/*!40000 ALTER TABLE `users_table` DISABLE KEYS */;
INSERT INTO `users_table` VALUES ('Admin','passw0rd',NULL);
/*!40000 ALTER TABLE `users_table` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-26 16:47:55
