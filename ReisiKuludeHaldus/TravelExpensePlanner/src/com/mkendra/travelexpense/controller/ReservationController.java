package com.mkendra.travelexpense.controller;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.mkendra.travelexpense.bean.Reservation;
import com.mkendra.travelexpense.service.ReservationService;

@Path("/reservations")
public class ReservationController {
	ReservationService reservationService = new ReservationService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Reservation> getReservations() {

		List<Reservation> listOfReservations = reservationService
				.getAllReservations();
		return listOfReservations;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Reservation getReservationById(@PathParam("id") int id) {
		return reservationService.getReservation(id);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Reservation addReservation(Reservation reservation) {
		return reservationService.addReservation(reservation);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Reservation updateReservation(Reservation reservation) {
		return reservationService.updateReservation(reservation);

	}

	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteReservation(@PathParam("id") int id) {
		reservationService.deleteReservation(id);
	}
}
