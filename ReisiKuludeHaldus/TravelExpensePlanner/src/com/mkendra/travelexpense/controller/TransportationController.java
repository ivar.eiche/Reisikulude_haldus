package com.mkendra.travelexpense.controller;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.mkendra.travelexpense.bean.Transportation;
import com.mkendra.travelexpense.service.TransportationService;

	@Path("/transportation")
	public class TransportationController {
		TransportationService transportationService = new TransportationService();

		@GET
		@Produces(MediaType.APPLICATION_JSON)
		public List<Transportation> getTransportations() {

			List<Transportation> listOfTransportations = transportationService
					.getAllTransportations();
			return listOfTransportations;
		}

		@GET
		@Path("/{id}")
		@Produces(MediaType.APPLICATION_JSON)
		public Transportation getTransportationById(@PathParam("id") int id) {
			return transportationService.getTransportation(id);
		}

		@POST
		@Produces(MediaType.APPLICATION_JSON)
		public Transportation addTransportation(Transportation transportation) {
			return transportationService.addTransportation(transportation);
		}

		@PUT
		@Produces(MediaType.APPLICATION_JSON)
		public Transportation updateTransportation(Transportation transportation) {
			return transportationService.updateTransportation(transportation);

		}

		@DELETE
		@Path("/{id}")
		@Produces(MediaType.APPLICATION_JSON)
		public void deleteTransportation(@PathParam("id") int id) {
			transportationService.deleteTransportation(id);
		}
	}
