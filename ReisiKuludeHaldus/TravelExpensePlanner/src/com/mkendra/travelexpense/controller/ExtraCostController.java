package com.mkendra.travelexpense.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.mkendra.travelexpense.bean.ExtraCost;
import com.mkendra.travelexpense.service.ExtraCostService;;

@Path("/extracost")
public class ExtraCostController {
	ExtraCostService extraCostService = new ExtraCostService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<ExtraCost> getAllExtraCosts() {
		List<ExtraCost> listOfExtraCosts = extraCostService.getAllExtraCosts();
		return listOfExtraCosts;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ExtraCost> getExtraCost(@PathParam("id") int id) {
		List<ExtraCost> extraCosts = extraCostService.getAllExtraCosts();
		return extraCosts;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public ExtraCost addExtraCost(ExtraCost extraCost) {
		return extraCostService.addExtraCost(extraCost);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public ExtraCost updateExtraCost(ExtraCost extraCost) {
		return extraCostService.updateExtraCost(extraCost);
	}

	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteExtraCost(@PathParam("id") int id) {
		extraCostService.deleteExtraCost(id);

	}

}
