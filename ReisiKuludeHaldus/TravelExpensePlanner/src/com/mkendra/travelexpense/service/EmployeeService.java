package com.mkendra.travelexpense.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mkendra.travelexpense.bean.Employee;

public class EmployeeService {

	public EmployeeService() {
		super();
	}

	public List<Employee> getAllEmployees() {
		List<Employee> employees = new ArrayList<Employee>();
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"Select * from reisikuludeplaneerimine.employees;")) {

			while (resultSet.next()) {

				employees.add(new Employee(resultSet.getInt("employeesID"),
						resultSet.getString("first_name"),
						resultSet.getString("last_name"),
						resultSet.getString("phone"),
						resultSet.getString("email"),
						resultSet.getString("social_security"),
						resultSet.getInt("occupationID"),
						resultSet.getString("department"),
						resultSet.getString("status")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return employees;
	}

	public Employee getEmployee(int id) {
		Employee employee = new Employee();
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"Select * from reisikuludeplaneerimine.employees where employeesID = "
								+ id + ";");) {

			while (resultSet.next()) {
				employee.setFirstName(resultSet.getString("first_name"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return employee;
	}

	public Employee addEmployee(Employee employee) {
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate(
					"Insert into reisikuludeplaneerimine.employees(first_name, last_name, phone, email, social_security, occupationID, department, status) VALUES ('"
							+ employee.getFirstName() + "', '"
							+ employee.getLastName() + "','"
							+ employee.getPhone() + "','" + employee.getEmail()
							+ "','" + employee.getSocialSecurity() + "','"
							+ employee.getProfession() + "','"
							+ employee.getDepartment() + "','"
							+ employee.getStatus() + "');");
		} catch (

		SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return employee;
	}

	public Employee deleteEmployee(int id) {
		Employee employee = new Employee();
		Connection connection = DBConnectionHandling.createConnection();

		String deleteTableSQL = "DELETE FROM ReisiKuludePlaneerimine.employees WHERE employeesID = "
				+ id + "";

		try (Statement statement = connection.createStatement();) {
			statement.execute(deleteTableSQL);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return employee;
	}

	public Employee updateEmployee(Employee employee) {
		Connection connection = DBConnectionHandling.createConnection();

		String updateTableSQL = "UPDATE reisikuludeplaneerimine.employees SET first_name='"
				+ employee.getFirstName() + "', last_name= '"
				+ employee.getLastName() + "', phone= '" + employee.getPhone()
				+ "', email='" + employee.getEmail() + "', social_security='"
				+ employee.getSocialSecurity() + "', occupationID="
				+ employee.getProfession() + ", department='"
				+ employee.getDepartment() + "', status='"
				+ employee.getStatus() + "' WHERE employeesID ="
				+ employee.getId() + ";";

		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate(updateTableSQL);
		} catch (

		SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return employee;
	}
}
