package com.mkendra.travelexpense.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mkendra.travelexpense.bean.Accommodation;

public class AccommodationService {

	public AccommodationService() {
		super();
	}

	public List<Accommodation> getAllAccommodations() {
		List<Accommodation> accommodation = new ArrayList<Accommodation>();
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"Select * from reisikuludeplaneerimine.accommodation;")) {

			while (resultSet.next()) {

				accommodation.add(
						new Accommodation(resultSet.getInt("accommodationID"),
								resultSet.getString("location"),
								resultSet.getString("check_in"),
								resultSet.getString("check_out"),
								resultSet.getBigDecimal("cost")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return accommodation;
	}

	public Accommodation getAccommodation(int id) {
		Accommodation accommodation = new Accommodation();
		Connection connection = DBConnectionHandling.createConnection();

		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"Select * from reisikuludeplaneerimine.accommodation where accommodationID = "
								+ id + ";");) {

			while (resultSet.next()) {
				accommodation.setId(resultSet.getInt("accommodationID"));
				accommodation.setLocation(resultSet.getString("location"));
				accommodation.setCheckIn(resultSet.getString("check_in"));
				accommodation.setCheckOut(resultSet.getString("check_out"));
				accommodation.setCost(resultSet.getBigDecimal("cost"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return accommodation;
	}

	public Accommodation addAccommodation(Accommodation accommodation) {
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate(
					"Insert into reisikuludeplaneerimine.accommodation(accommodationID, location, check_in, check_out, cost) VALUES ('"
							+ accommodation.getId() + "', '"
							+ accommodation.getLocation() + "','"
							+ accommodation.getCheckIn() + "','"
							+ accommodation.getCheckOut() + "','"
							+ accommodation.getCost() + "');");
		} catch (

		SQLException e) {
			e.printStackTrace();
		}

		DBConnectionHandling.closeConnection(connection);
		return accommodation;
	}

	public Accommodation deleteAccommodation(int id) {
		Accommodation accommodation = new Accommodation();
		Connection connection = DBConnectionHandling.createConnection();

		String deleteTableSQL = "DELETE FROM ReisiKuludePlaneerimine.accommodation WHERE accommodationID = "
				+ id + "";

		try (Statement statement = connection.createStatement();) {
			statement.execute(deleteTableSQL);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return accommodation;
	}

	public Accommodation updateAccommodation(Accommodation accommodation) {
		Connection connection = DBConnectionHandling.createConnection();

		String updateTableSQL = "UPDATE reisikuludeplaneerimine.accommodation SET location='"
				+ accommodation.getLocation() + "', check_in= '"
				+ accommodation.getCheckIn() + "', check_out= '"
				+ accommodation.getCheckOut() + "', cost ='"
				+ accommodation.getCost() + "'  WHERE accommodationID = "
				+ accommodation.getId() + ";";

		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate(updateTableSQL);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return accommodation;
	}
}