package com.mkendra.travelexpense.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import com.mkendra.travelexpense.bean.Reservation;

public class ReservationService {

	public ReservationService() {
		super();
	}

	public List<Reservation> getAllReservations() {
		List<Reservation> reservations = new ArrayList<Reservation>();

		// reservationMap.values()
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"Select * from reisikuludeplaneerimine.reservations;")) {

			while (resultSet.next()) {

				reservations
						.add(new Reservation(resultSet.getInt("reservationsID"),
								resultSet.getString("first_name"),
								resultSet.getString("last_name"),
								resultSet.getString("departure"),
								resultSet.getString("destination"),
								resultSet.getString("start_date"),
								resultSet.getString("end_date"),
								resultSet.getString("transportation_type"),
								resultSet.getInt("transportation_cost"),
								resultSet.getString("accommodation"),
								resultSet.getInt("accommodation_cost"),
								resultSet.getString("trip_description"),
								resultSet.getInt("estimated_cost")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return reservations;
	}

	public Reservation getReservation(int id) {
		Reservation reservation = new Reservation();
		Connection connection = DBConnectionHandling.createConnection();

		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"Select * from reisikuludeplaneerimine.reservations where reservationsID = "
								+ id + ";");) {

			while (resultSet.next()) {
				reservation.setId(resultSet.getInt("reservationsID"));
				reservation.setFirstName(resultSet.getString("first_name"));
				reservation.setLastName(resultSet.getString("last_name"));
				reservation.setDeparture(resultSet.getString("departure"));
				reservation.setDestination(resultSet.getString("destination"));
				reservation.setDestination(resultSet.getString("start_date"));
				reservation.setEndDate(resultSet.getString("end_date"));
				reservation.setTransportationType(
						resultSet.getString("transportation_type"));
				reservation.setTransportationCost(
						resultSet.getInt("transportation_cost"));
				reservation
						.setAccommodation(resultSet.getString("accommodation"));
				reservation.setAccommodationCost(
						resultSet.getInt("accommodation_cost"));
				reservation.setTripDescription(
						resultSet.getString("trip_description"));
				reservation
						.setEstimatedCost(resultSet.getInt("estimated_cost"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return reservation;
	}

	public Reservation addReservation(Reservation reservation) {
		Connection connection = DBConnectionHandling.createConnection();

			try (Statement statement = connection.createStatement();) {
				statement.executeUpdate(
						"Insert into reisikuludeplaneerimine.reservations(first_name, last_name, departure, destination, start_date, end_date, transportation_type, transportation_cost, accommodation, accommodation_cost, trip_description, estimated_cost) VALUES ('"
								+ reservation.getFirstName() + "', '"
								+ reservation.getLastName() + "','"
								+ reservation.getDeparture() + "','"
								+ reservation.getDestination() + "','"
								+ reservation.getStartDate() + "','"
								+ reservation.getEndDate() + "','"
								+ reservation.getTransportationType() + "',"
								+ reservation.getTransportationCost() + ",'"
								+ reservation.getAccommodation() + "',"
								+ reservation.getAccommodationCost() + ",'"
								+ reservation.getTripDescription() + "',"
								+ reservation.getEstimatedCost() + ");");
			} catch (

			SQLException e) {
				e.printStackTrace();
			}
		DBConnectionHandling.closeConnection(connection);
		return reservation;
	}

	public Reservation deleteReservation(int id) {
		Reservation reservation = new Reservation();
		Connection connection = DBConnectionHandling.createConnection();

		String deleteTableSQL = "DELETE FROM ReisiKuludePlaneerimine.reservations WHERE reservationsID = "
				+ id + "";

		try (Statement statement = connection.createStatement();) {
			statement.execute(deleteTableSQL);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return reservation;
	}

	public Reservation updateReservation(Reservation reservation) {
		Connection connection = DBConnectionHandling.createConnection();

		String updateTableSQL = "UPDATE reisikuludeplaneerimine.reservations SET first_name='"
				+ reservation.getFirstName() + "', last_name= '"
				+ reservation.getLastName() + "', departure= '"
				+ reservation.getDeparture() + "', destination='"
				+ reservation.getDestination() + "', start_date='"
				+ reservation.getStartDate() + "', end_date= '"
				+ reservation.getEndDate() + "', transportation_type='"
				+ reservation.getTransportationType()
				+ "', transportation_cost = "
				+ reservation.getTransportationCost() + ",accommodation='"
				+ reservation.getAccommodation() + "',accommodation_cost = "
				+ reservation.getAccommodationCost() + ",trip_description = '"
				+ reservation.getTripDescription()
				+ "'  WHERE reservationsID = " + reservation.getId() + ";";

		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate(updateTableSQL);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return reservation;
	}
}