package com.mkendra.travelexpense.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mkendra.travelexpense.bean.Transportation;

public class TransportationService {

	public TransportationService() {
		super();
	}

	public List<Transportation> getAllTransportations() {
		List<Transportation> transportation = new ArrayList<Transportation>();
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"Select * from reisikuludeplaneerimine.transportation;")) {

			while (resultSet.next()) {

				transportation.add(
						new Transportation(resultSet.getInt("transportationID"),
								resultSet.getString("transportation_type"),
								resultSet.getString("transportation_from"),
								resultSet.getString("transportation_to"),
								resultSet.getInt("cost")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return transportation;
	}

	public Transportation getTransportation(int id) {
		Transportation transportation = new Transportation();
		Connection connection = DBConnectionHandling.createConnection();

		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"Select * from reisikuludeplaneerimine.transportation where transportationID = "
								+ id + ";");) {

			while (resultSet.next()) {
				transportation.setId(resultSet.getInt("transportationID"));
				transportation.setTransportationType(
						resultSet.getString("transportation_type"));
				transportation.setTransportationFrom(
						resultSet.getString("transportation_from"));
				transportation.setTransportationTo(
						resultSet.getString("transportation_to"));
				transportation.setCost(resultSet.getInt("cost"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return transportation;
	}

	public Transportation addTransportation(Transportation transportation) {
		Connection connection = DBConnectionHandling.createConnection();

		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate(
					"Insert into reisikuludeplaneerimine.transportation(transportationID, transportation_type, transportation_from, transportation_to, cost) VALUES ("
							+ transportation.getId() + ", '"
							+ transportation.getTransportationType() + "','"
							+ transportation.getTransportationFrom() + "','"
							+ transportation.getTransportationTo() + "',"
							+ transportation.getCost() + ");");
		} catch (

		SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return transportation;
	}

	public Transportation deleteTransportation(int id) {
		Transportation transportation = new Transportation();
		Connection connection = DBConnectionHandling.createConnection();

		String deleteTableSQL = "DELETE FROM ReisiKuludePlaneerimine.transportation WHERE transportationID = "
				+ id + "";

		try (Statement statement = connection.createStatement();) {
			statement.execute(deleteTableSQL);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return transportation;
	}

	public Transportation updateTransportation(Transportation transportation) {
		Connection connection = DBConnectionHandling.createConnection();

		String updateTableSQL = "UPDATE reisikuludeplaneerimine.transportation SET transportation_type='"
				+ transportation.getTransportationType()
				+ "', transportation_from= '"
				+ transportation.getTransportationFrom()
				+ "', transportation_to= '"
				+ transportation.getTransportationTo() + "', cost="
				+ transportation.getCost()
				+ "  WHERE transportationID = " + transportation.getId() + ";";

		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate(updateTableSQL);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return transportation;
	}
}
