package com.mkendra.travelexpense.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.mkendra.travelexpense.bean.ExtraCost;

public class ExtraCostService {

	public ExtraCostService() {
		super();
	}

	public List<ExtraCost> getAllExtraCosts() {
		List<ExtraCost> extraCost = new ArrayList<ExtraCost>();
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"Select * from reisikuludeplaneerimine.extra_cost;")) {

			while (resultSet.next()) {

				extraCost.add(new ExtraCost(resultSet.getInt("extraID"),
						resultSet.getString("extra_type"),
						resultSet.getBigDecimal("extra_price"),
						resultSet.getString("extra_date"),
						resultSet.getInt("reservationID")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return extraCost;
	}

	public List<ExtraCost> getExtraCost (int reservationId) {
		List<ExtraCost> extraCosts = new ArrayList<ExtraCost>();
		Connection connection = DBConnectionHandling.createConnection();

		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"Select * from reisikuludeplaneerimine.extra_cost where reservationID = "
								+ reservationId + ";");) {

			while (resultSet.next()) {
			extraCosts.add(new ExtraCost(resultSet.getInt("extraID"), 
						resultSet.getString("extra_type"), resultSet.getBigDecimal("extra_price"),
						resultSet.getString("exptra_date"), resultSet.getInt("reservationIT")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return extraCosts;
	}

	public ExtraCost addExtraCost(ExtraCost extraCost) {
		Connection connection = DBConnectionHandling.createConnection();

		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate(
					"Insert into reisikuludeplaneerimine.extra_cost(extraID, extra_type, extra_price, extra_date, reservationID) VALUES ("
							+ extraCost.getExtraCostId() + ", '"
							+ extraCost.getExtraCostType() + "','"
							+ extraCost.getExtraCostPrice() + "','"
							+ extraCost.getExtraCostDate() + "', "
							+ extraCost.getReservationId() + ");");
		} catch (

		SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return extraCost;
	}

	public ExtraCost deleteExtraCost(int id) {
		ExtraCost extraCost = new ExtraCost();
		Connection connection = DBConnectionHandling.createConnection();

		String deleteTableSQL = "DELETE FROM ReisiKuludePlaneerimine.extra_cost WHERE extraCostID = "
				+ id + "";

		try (Statement statement = connection.createStatement();) {
			statement.execute(deleteTableSQL);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return extraCost;
	}

	public ExtraCost updateExtraCost(ExtraCost extraCost) {
		Connection connection = DBConnectionHandling.createConnection();

		String updateTableSQL = "UPDATE reisikuludeplaneerimine.extra_cost SET extra_type='"
				+ extraCost.getExtraCostType() + "', extra_price= "
				+ extraCost.getExtraCostPrice() + ", extra_date= '"
				+ extraCost.getExtraCostDate() + "'  WHERE extraCostID = "
				+ extraCost.getExtraCostId() + ";";

		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate(updateTableSQL);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return extraCost;
	}
}
