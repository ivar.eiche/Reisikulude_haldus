package com.mkendra.travelexpense.bean;

public class Manager extends Person {

	private String department;
	
	public Manager(){
		super();
	}

	public Manager(int id, String firstName, String lastName, String phone,
			String email, String socialSecurity, int profession, String department) {
		super(id, firstName, lastName, phone, email, socialSecurity, profession);
		this.department = department;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
}