package com.mkendra.travelexpense.bean;

import java.math.BigDecimal;

public class Accommodation {
	int id;
	String location;
	String checkIn;
	String checkOut;
	BigDecimal cost;

	public Accommodation() {
	}

	public Accommodation(int i, String location, String checkIn,
			String checkOut, BigDecimal cost) {
		this.id = i;
		this.location = location;
		this.checkIn = checkIn;
		this.checkOut = checkOut;
		this.cost = cost;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(String checkIn) {
		this.checkIn = checkIn;
	}

	public String getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
}
