package com.mkendra.travelexpense.bean;

public class Transportation {

	int id;
	String transportationType;
	String transportationFrom;
	String transportationTo;
	int cost;

	public Transportation() {
	}

	public Transportation(int i, String transportationType,
			String transportationForm, String transportationTo,
			int cost) {
		this.id = i;
		this.transportationType = transportationType;
		this.transportationFrom = transportationForm;
		this.transportationTo = transportationTo;
		this.cost = cost;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTransportationType() {
		return transportationType;
	}

	public void setTransportationType(String transportationType) {
		this.transportationType = transportationType;
	}

	public String getTransportationFrom() {
		return transportationFrom;
	}

	public void setTransportationFrom(String transportationFrom) {
		this.transportationFrom = transportationFrom;
	}

	public String getTransportationTo() {
		return transportationTo;
	}

	public void setTransportationTo(String transportationTo) {
		this.transportationTo = transportationTo;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}
}
