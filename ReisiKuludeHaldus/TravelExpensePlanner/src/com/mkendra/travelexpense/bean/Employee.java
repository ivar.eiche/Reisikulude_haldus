package com.mkendra.travelexpense.bean;

public class Employee extends Person {

	private String department;
	private String status;

	public Employee() {
		super();
	}

	public Employee(int id, String firstName, String lastName, String phone,
			String email, String socialSecurity, int profession,
			String department, String status) {
		super(id, firstName, lastName, phone, email, socialSecurity, profession);
		this.department = department;
		this.status = status;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}