var allEmployees;
function getAllEmployees() {
	$
			.getJSON(
					"http://localhost:8080/TravelExpensePlanner/rest/employees",
					function(employees) {
						allEmployees = employees;
						var tableContent = "";
						for (var i = 0; i < employees.length; i++) {
							tableContent = tableContent
									+ "<tr><td>"
									+ employees[i].firstName
									+ "</td><td>"
									+ employees[i].lastName
									+ "</td><td>"
									+ employees[i].phone
									+ "</td><td>"
									+ employees[i].email
									+ "</td><td>"
									+ employees[i].socialSecurity
									+ "</td><td>"
									+ employees[i].profession
									+ "</td><td>"
									+ employees[i].department
									+ "</td><td>"
									+ employees[i].status
									+ "</td><td>"
									+ '<button type="button" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#myModal" data-whatever="'
									+ i
									+ '">Edit</button>'
									+ "</td><td>"
									+ '<button type ="button" class = "btn btn-danger" onClick="deleteEmployee('
									+ employees[i].id + ')">Delete</button>'
									+ "</td></tr>";
						}
						document.getElementById("employeeTableBody").innerHTML = tableContent;
					});
}
getAllEmployees();

function updateEmployee() {
	input_box = confirm("Are you sure you want to update this info?");
	if (input_box == true) {

		var updateEmployeeData = JSON.stringify($("#userForm")
				.serializeFormJSON());
		$
				.ajax({
					url : "http://localhost:8080/TravelExpensePlanner/rest/employees",
					cache : false,
					type : 'PUT',
					data : updateEmployeeData,
					success : function(updateEmployeeData) {
						location.reload();
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						console.log("Status: " + textStatus);
						console.log("Error: " + errorThrown);
					},
					contentType : "application/json; charset=utf-8"

				});

	} else {
		return false;
	}
};

function deleteEmployee(employeesID) {
	input_box = confirm("Are you sure you want to delete this Employee?");
	if (input_box == true) {
		$

				.ajax({
					url : "http://localhost:8080/TravelExpensePlanner/rest/employees/"
							+ employeesID,
					cache : false,
					type : 'DELETE',
					success : function(DeleteEmployeeData) {
						location.reload();
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						console.log("Status: " + textStatus);
						console.log("Error: " + errorThrown);
					},
					contentType : "application/json; charset=utf-8"
				});
	} else {
		return false;
	}
};


$('form').submit(function(event) {
	event.preventDefault();
	var newEmployeeData = JSON.stringify($(this).serializeFormJSON());
	console.log(newEmployeeData);

	$.ajax({
		url : "http://localhost:8080/TravelExpensePlanner/rest/employees",
		cache : false,
		type : 'POST',
		data : newEmployeeData,
		success : function(newEmployeeData) {
			location.reload();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		},
		contentType : "application/json; charset=utf-8"

	});
});

(function($) {
	$.fn.serializeFormJSON = function() {

		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};
})(jQuery);

$('#myModal')
.on(
		'show.bs.modal',
		function(event) {
			var button = $(event.relatedTarget)
			var recipient = button.data('whatever')
			var oneEmployee;
			function getEmployee(employeesID) {
				$
						.getJSON(
								"http://localhost:8080/TravelExpensePlanner/rest/employees/"
										+ employeesID,
								function(employees) {
									oneEmployee = employees;
									var tableContent2 = "";
									{
										tableContent2 = tableContent
												+ "<tr><td>"
												+ employees.id
												+ "</td><td>"
												+ employees.firstName
												+ "</td><td>"
												+ employees.lastName
												+ "</td><td>"
												+ employees.phone
												+ "</td><td>"
												+ employees.email
												+ "</td><td>"
												+ employees.socialSecurity
												+ "</td><td>"
												+ employees.profession
												+ "</td><td>"
												+ employees.department
												+ "</td><td>"
												+ employees.status
												+ "</td></tr>";
									}
									document
											.getElementById("employeeTableBody").innerHTML = tableContent;
								});
			}
			var modal = $(this)
			modal.find('.modal-body input#id').val(
					allEmployees[recipient].id)
			modal.find('.modal-body input#firstName').val(
					allEmployees[recipient].firstName)
			modal.find('.modal-body input#lastName').val(
					allEmployees[recipient].lastName)
			modal.find('.modal-body input#phone').val(
					allEmployees[recipient].phone)
			modal.find('.modal-body input#email').val(
					allEmployees[recipient].email)
			modal.find('.modal-body input#socialSecurity').val(
					allEmployees[recipient].socialSecurity)
			modal.find('.modal-body input#profession').val(
					allEmployees[recipient].profession)
			modal.find('.modal-body input#department').val(
					allEmployees[recipient].department)
			modal.find('.modal-body input#status').val(
					allEmployees[recipient].status)
		})